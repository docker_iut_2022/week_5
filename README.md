# week_5

Introduction to Kubernetes
1. [What is Kubernetes?](#1)

	2. [Definition and history](#2)

	3. [Concepts and internal structure](#3)

	4. [What Kubernetes will be tomorrow: bare-metal k8s](#4)

	5. [A few numbers about Kubernetes](#5)

	6. [What people are running on Kubernetes clusters](#6)

	7. [Kubernetes distributions](#7)

8. [How to install Kubernetes](#8)

	9. [Vanilla Kubernetes installation](#9)

	10. [MicroK8s managed Kubernetes installation](#10)

11. [First examples](#11)

12. [Second example: guestbook application](#12)


## What is Kubernetes? <a name="1"></a>

![alt text](pictures/what_is_kubernetes.jpg)

### Definition and history <a name="2"></a>

*Kubernetes* comes from the greek work meaning *helmsman*, the one who steers a (container) ship. Fun fact: it also gave us *cybernetics*, the science of communication and automatic control systems. **Kubernetes** is often written **k8s** (pronounce *coo-ber-net-ees*).

![alt text](pictures/kubernetes_logo.png)

*Kubernetes icon is a stylized ship helm*

[Kubernetes](https://kubernetes.io/) is an open-source system for automated deployment, scaling and management of containerized applications. It is the **highest layer of abstraction between applications and server machines**. 

Using Kubernetes, a customer renting server resources does not even need to know what his application will be running on, only the desired amount of processing time/space. Server maintenance / configuration is hidden behind the **Kubernetes API** (version 1.24 release planned in April 2022). 

It was initially a **Google project** started in June 2014, with the 1.0 version released in July 2015. It was heavily influenced by Google's internal existing solution for managing containers, named [Borg](https://en.wikipedia.org/wiki/Borg_(cluster_manager)). Google partenered with the Linux Foundation to form the **Cloud Native Computing Foundation** ([CNCF](https://www.cncf.io/)) and offered it Kubernetes property. 

In February 2016, a package manager named [Helm](https://helm.sh/) was released for Kubernetes. It is the [equivalent of apt](https://www.bmc.com/blogs/kubernetes-helm-charts/) for Kubernetes-ready applications which can be deployed as one unit. **Kubernetes applications are configured with YAML manifests**, and Helm is abstracting the maintenance and writing of these files to keep deployments as straighforward as possible.

Google published a pretty comprehensive sales-pitch in a [comics](https://cloud.google.com/kubernetes-engine/kubernetes-comic) format about what Kubernetes is about exactly. 

### Concepts and internal structure <a name="3"></a>

![alt text](pictures/Kubernetes_overview.png)

*Image: versusmind.eu*

* Kubernetes has a **master / slave architecture**, with a master service launched initially and worker services added as the needs increase. The whole set of services running and interacting through the Kubernetes API is called a **cluster**.
* **Control plane**: Kubernetes master component, in charge of maintaining the workload and communication within the cluster. Each subsystem is a separate process, which runs on a single master or even more in high-availability clusters:
	* **etcd**, a persistent and distributed data store for the cluster configuration.
	* The **API server**, processing the **Kubernetes API requests** using JSON over HTTP and REST requests. This server manages **internal and external requests** for its cluster. The API server uses etcd to monitor the cluster and detect configuration changes or divergences from the desired state. If needed, it plans corrective actions to **match the current configuration**.
	* The **scheduler** decides which node a pod is going to run on. It is responsible with **distributing the workload** (demand) with the available resources (supply).
	* The **controller** is communicating with the API server to **apply the required actions** to get the cluster toward the desired state. It creates, updates and delete resources. Several kind of controllers exist:
		* **Replication controller**: for replication and scaling, like when a given number of copies of a pod are requested on its cluster. This is the one creating replacement pods when the node hosting them is failing.
		* **DaemonSet controller**: a dedicated type of controller to ensure exactly one given pod is running on all selected machines.
		* **Job controller**: for running batches of pods with a finite lifespan to completion. 
	* The **controller manager**: process handling a set of core Kubernetes controllers.
* **Node**: originally called *minions*, nodes are the representations of physical or virtual machines. Each node is made of 3 main subsystems:
	* **Kubelet, the node manager**: it starts, stops, and monitors pods following the control plane instructions. Every few seconds, it reports the node state to the master node. An unhealthy pod in this node will be relaunched locally. If it stops responding, pods are re-deployed on other nodes by the replication controller.
	* **Kube-proxy**: a load balancer and network virtual proxy to route traffic to the right container depending on the IP, port and request type. 
	* **Containers**! they run inside a pod, and host applications, libraries and dependencies. They can be assigned an external IP address to be exposed publicly. Docker has been the official container runtime from Kubernetes beginning, but this is slowly changing. After version 1.20, Kubernetes deprecated the use of Docker as a container runtime. Why?
	 
	  ![alt text](pictures/container-ecosystem.png)
	  
	  *Image: tutorialworks.com*

	  The **Container Runtime Interface (CRI)** is the **API between Kubernetes and container runtimes**. **Docker** runtime is **not compliant** with it, as it is older than Kubernetes. Under the hood, Docker uses **containerd**, which *is* CRI compliant, but to access it through Docker stack, Kubernetes has to use an intermediate called **Dockershim**. In the beginning Docker was the only reliable container runtime and containerd didn't exist as a separate entity, so **Kubernetes had to carry all the human-focused part coming with Docker, that it did not use**, around the actual container runtime it needed. Now **standardized alternatives** are available, like [CRI-O](https://cri-o.io/), [Karaf](https://karaf.apache.org/), [Ondat](https://www.ondat.io/), rkt (remember [rkt](https://github.com/rkt/rkt/)?).
* **Pod**: The **smallest scheduling unit** in Kubernetes. It is made of one or more containers, located inside the same node. **Pods all have a unique IP** to separate applications using the same ports without conflicts. Inside a pod, containers can interact between them, and using a pod IP address, they can also reach containers from another pod. These IP are usually (very) dynamic, and instead of using them, it is **common to point to references to a service**.
  Pods can be attached to volumes and expose them to their containers. They can be managed manually via the Kubernetes API, but most of the time it is a controller's job to to it. 
  Pods can be declared as a part of a DaemonSet, which mandates to have one of them on every nodes, or as ReplicaSets, which mandates a given amount to run at any time.
* **Namespaces**: Kubernetes is segregating resources with separate sets called namespaces. They allow to separate teams, development, tests and production targets.
* **Services**: A Kubernetes service is a set of pods working together. Pods making a service are tagged with a label selector. Kubernetes can detect services either via its DNS, or using environment variables. Services have their own IP address and name, and can be exposed outside the cluster.
* **Volumes**: pods filesystems are wiped on restart/end, and Kubernetes volumes are allowing for persistent data in a pod, in a very similar way to docker volumes. They are defined in the pod configuration.
* **ConfigMaps** and **Secrets**: these are **dictionaries of values** containing pods configurations. **Only the nodes hosting the related pods will get these**, following a **need-to-know policy**, and when the pod is removed from the node, related configMaps/secrets are also removed. **The data is coming from the master node**, which is considered **secured**, and the main difference between them is the **encoding**: **secrets are base64 encoded**, while configMaps are plain text data. 
* **StatefulSets**: some applications do not require to keep a trace of their current state (*stateless* apps) when restarting the pod, but some apps like database do require to distribute their state to other pods when scaling the application. Typically, a database is often duplicated with primary and secondary instances. A statefulSet is a controller dedicated to the management of such pods where their instances have unique properties / an ordered relationship.
* **Deployments**: higher level management mechanism for ReplicaSets. Deployments define the behavior on a replica set when scaling it up or down, and when updating or rolling back the pods version.
* **Labels and selectors**: Kubernetes objects are labeled and it is possible to choose the objects to interact with using label selectors, which are queries on labels. Field selector also exist and fill the same purpose.
* **Addons**: they are running like other apps as pods in the cluster, but extends the Kubernetes cluster features. Common addons are:
	* DNS. Cluster DNS is a mandatory feature, and containers inside Kubernetes automatically include this DNS server in their DNS searches.
	* Web UI: a web interface between the cluster and users to monitor and handle applications or the cluster itself.
	* Container Resource Monitoring: record metrics about containers workload in a central database, and provides a UI to browse it.
	* Cluster-level logging: logs can be lost if they are dependent on a container, pod or node. This addon centralise logs in a browsable cluster log store, as Kubernetes provides no native storage for log data.
* **Storage**: implementing persistent storage for containers is one of the top challenges of Kubernetes administrators. Today a common implementation for storage is to use **Container Attached Storage** ([CAS](https://www.cncf.io/blog/2018/04/19/container-attached-storage-a-primer/)), a microservice.


This about sums it up for Kubernetes key concepts. That was not that hard, was it?

![alt text](pictures/k8s_rabbit_hole.png)


Of course that was not all of it, there is also the **Container Network Interface** ([CNI](**http://**)), which is the component settings up all these network interfaces between pods and containers, and [service meshes](https://avinetworks.com/glossary/kubernetes-service-mesh/) to abstract communication between micro-services, and ...

![alt text](pictures/Edvard_Munch_The_Scream.jpg)

*Image: Edward Munch, The Scream, 1910, Wikimedia Commons*

### What Kubernetes will be tomorrow: bare-metal k8s <a name="4"></a>

Most servers infrastructures were virtualized for practical reasons in the early 2010s. Even before container orchestration became mainstream, most containers were already running on these virtual machines. Kubernetes naturally colonised this environment and took over container management running its nodes on VMs, but **increasing concerns about security and the desired for less overhead** and better performances are driving the current mutation of Kubernetes: going [bare-metal](https://ubuntu.com/blog/understanding-bare-metal-kubernetes), and **running directly on physical machines**.

![alt text](/home/raph/iut/week_5/pictures/linkedin_poll_kubernetes.png)

*Image: Canonical poll from LinkedIn, December 2021*

The reasoning is that most security failure-points are now software, and skipping as much software components as possible is reducing the area vulnerable to attacks.

Even if bare-metal container orchestration existed way before Kubernetes, with services like [Machine as a Service](https://maas.io/) (Maas) provided by Canonical as early as 2012, they lacked the standardization of technologies and interface to become mainstream. As these obstacles slowly disappeared, the share of bare-metal kubernetes clusters is increasing. 

Another reason for the adoption of bare-metal clusters is the emergence of very specialized hardware (like smart Network Interface Cards), and its direct access: offloading computation operations to GPUs, acceleration techns like smart Network Interface Cards, are a [lot more effective](https://www.ericsson.com/en/blog/2020/3/benefits-of-kubernetes-on-bare-metal-cloud-infrastructure) on bare-metal Kubernetes.

Unfortunately, they are not always simpler than VM clusters...

![alt text](pictures/k8s-meme.jpg)


### A few numbers about Kubernetes <a name="5"></a>

Kubernetes is an active open-source project, with a [very active](https://www.cncf.io/wp-content/uploads/2022/02/CNCF-AR_FINAL-edits-15.2.21.pdf) community:
* As of 2021, **96% of respondents** in the Cloud Native Computing Foundation **reported using or testing Kubernetes**. 69% are already using it in production.
* 31% of backend developers around the world are using Kubernetes.
* Between 2017 and 2020, [Datadog](https://www.datadoghq.com/container-report-2020/)'s percent of customers using Kubernetes went from 25 to 50%.
* 85% percent of IT leaders think Kubernetes is at least important to cloud-native strategies, and 72% expect to increase their container usage over the next year (according to a 2021 Red Hat [survey](https://www.redhat.com/en/enterprise-open-source-report/2022?intcmp=701f2000000tjyaAAA)).
* Kubernetes is the [second most starred](https://github.com/EvanLi/Github-Ranking/blob/master/Top100/Go.md) Go project on Github, only behind..[Go](https://github.com/golang/go) language repository itself. Most modern containerization technologies (Docker + Kubernetes) are written using the Go language. 
* Between 2018 and 2020, Kubernetes was in the top 10 most active projects by number of commits, and even second by number of issues, [only behind](https://web.archive.org/web/20181029081848/https://www.cncf.io/blog/2018/03/06/kubernetes-first-cncf-project-graduate/) the Linux kernel.
* How many containers are being run everyday? It is difficult to get even a rough estimation, but in 2014, which was container orchestration dark ages, **Google** [reported](https://www.theregister.com/2014/05/23/google_containerization_two_billion/) creating **2 billions containers a week**. That is more than 3300 *per second*, and since then Kubernetes made this number increase rapidly every year. 

### What people are running on Kubernetes clusters <a name="6"></a>

![alt text](pictures/top_image_with_kubernetes.png)
*Image: Datatog 2019 customer survey*

1.  Redis: database
2.  ElasticSearch: search engine
3.  PostGres: database
4.  RabbitMQ: messenging system
5.  Kafka: data and event processing
6.  Vault: API for secured data exchange
7.  Mongo: database
8.  Nginx: generic web server, proxy, http cache and load balancer
9.  Consul: networking
10.  MySQL: database

### Kubernetes distributions <a name="7"></a>

Kubernetes is dominating the container management amrket today (March 2022):

![alt text](pictures/kubernetes_market_share.jpg)

*Image: trlogic.com*

Openshift is actually a Kubernetes variant, and Swarm is Docker historical solution, now deprecated. But Kubernetes is not a monolith: you can choose for example to just install the minimal set of tools to run Kubernetes: Kubeadm, Kubelet and kube-proxy (More on these later). 

This is called **vanilla Kubernetes**. In this case a company has to build a Kubernetes platform from the bare tools, and maintain it. The (*much preferred*) alternative is to use pre-compiled and pre-configured **managed Kubernetes**, also called **Kubernetes distribution**.

![alt text](pictures/kubernetes_distrib_chart.png)

Kubernetes is a *very* big, open-source [project](https://github.com/kubernetes/kubernetes), and like most of them has been forked into distributions by the cloud market main actors:

* **AKS**: **Azure Kubernetes Service**. Originally named Azure Container Service until June 2018 because it was also supporting Apache Mesos and Docker Swarm. This is Microsoft solution. AKS is usually the **most cost-effective solution** and can be easily integrated with Microsoft other tools. 
* **EKS**: Amazon's **Elastic Kubernetes Service**. like Microsoft, they started with a solution named Elastic Container Service, then renamed it in June 2018 when Kubernetes clearly outgrew the competition. Amazon's EKS is the **most widely used** Kubernetes service today.
* **GKE**: **Google Kubernetes Engine**. The **first cloud provider to release a Kubernetes Engine** was Google in 2015, largely because it started as an internal project of the company. GKE is the service **providing the most features** and automated capabilities.

There are also [lighter](https://blog.flant.com/small-local-kubernetes-comparison/) distributions, used by smaller entities and developers to experiment or maintain Kubernetes clusters on a more local scale:
* [kOs](https://k0sproject.io/): a Linux-based, single binary project created in 2020 and compatible with k8s versions > 1.20.
* [MicroK8s](https://microk8s.io/): created in 2018, this mini-cluster supports 42 Linux distributions and is using Calico as its Cluster Networking Interface (CNI). Its features can be expanded with addons. k8s versions > 1.19 are supported.
* [kind](https://kind.sigs.k8s.io/): also in 2018, this one is special. Kind stands for **K**ubernetes **in D**ocker, and in this cluster the nodes are containers. The default CNI is *kindnetd*, and no addons/plugins are supported. Only 1.21 k8s version is supported for now. This cluster has been at least partially integrated to run within the WSL2. 
* [k3s](https://k3s.io/) and [k3d](https://k3d.io/): k3s has been developed by Rancher in 2019 and supports 1.17 and later versions of k8s. This distribution focuses on having a small memory footprint. K3d is a parallel utility used to manage k3s nodes running in a Docker container. 
* [Minikube](https://minikube.sigs.k8s.io/): the ancestor, active since April 2016 and supporting all k8s version after 1.11. A lot of existing CNI are available as plugins.

![alt text](pictures/summary_small_cluster_features.png)

*Image: blog.flant.com*



## How to install Kubernetes <a name="8"></a>

![alt text](pictures/how-you-deploy-a-container-on-kubernetes.jpg)

*Image: globalnerdy.com*

### Vanilla Kubernetes installation <a name="9"></a>

*If you plan on using a managed Kubernetes (which is recommended), you can skip this section.*

Kubernetes environment requires at least three packages to be installed:
* **kubeadm**: the command to bootstrap the cluster
* **kubelet**: the component running on all the machines in the cluster. This is the one starting the pods and containers.
* **kubectl**: The commande line interface to to manage your cluster.

On Ubuntu 20.04, you can setup kubernetes components from Google servers:

To add the packages to your apt index:

```
sudo apt -y install curl apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt update
```

To install them:

```
sudo apt -y install vim git curl wget kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl
```

### MicroK8s managed Kubernetes installation <a name="10"></a>

On Ubuntu 20.04, you can setup kubernetes with the installer package `kubernetes`:

```
sudo apt install kubernetes
```

It will ask the **Kubernetes distribution** you want to install:

```
kubernetes install
1) microk8s
2) Canonical Distribution of Kubernetes
3) Quit
Please choose an installation option: 1
Fetching components...
conjure-up 2.6.14-20200716.2107 from Canonical✓ installed
```

Most distributions of Kubernetes are designed to run on clouds infrastructures, or bare-metal (read: not a virtual machine). **MicroK8s** is a lighter distribution designed to run small clusters on developers machines with a great portability. It also has an increasing share of small machines deployments on [edge sites](https://www.techtarget.com/searchcloudcomputing/news/252469673/Microsoft-Azure-edge-site-expansion-targets-cloud-latency), where redundancy is not required / too expensive, but Kubernetes API is still useful for managing its life cycle.

Once MicroK8s is installed, you can add your user to its group to avoid typing all commands as root:

```
# Add current user to the microk8s group
sudo usermod -a -G microk8s $USER
# Set permissions on microk8s config folder
sudo chown -f -R $USER ~/.kube
# Force group rules to be reloaded
newgrp microk8s
```


You can now check Kubernetes status with the command:

```
microk8s status --wait-ready
microk8s is running
high-availability: no
  datastore master nodes: 127.0.0.1:19001
  datastore standby nodes: none
addons:
  enabled:
    ha-cluster           # Configure high availability on the current node
```

You can get microk8s complete subcommands list with:

```
microk8s help
Available subcommands are:
	add-node
	cilium
	config
	ctr
	dashboard-proxy
	dbctl
	disable
	enable
	helm3
	helm
	istioctl
	join
	juju
	kubectl
	leave
	linkerd
	refresh-certs
	remove-node
	reset
	start
	status
	stop
	inspect
```


## First examples <a name="11"></a>

You can first add basic services, like a DNS resolver and a web-based dashboard to interact with Kubernetes:

```
microk8s enable dns dashboard
```

To access Kubernetes dashboard, you need a token and the server address.
To get the token:

```
token=$(microk8s kubectl -n kube-system get secret | grep default-token | cut -d " " -f1)
microk8s kubectl -n kube-system describe secret $token
```

To get the server adress, look for `service/kubernetes-dashboard` in Kubernetes namespaces list. To print it, use the command `microk8s kubectl get all --all-namespaces`:

```
microk8s kubectl get all --all-namespaces
NAMESPACE     NAME                                             READY   STATUS    RESTARTS        AGE
kube-system   pod/coredns-64c6478b6c-qm8mz                     1/1     Running   0               2m29s
kube-system   pod/calico-node-cctmh                            1/1     Running   1 (5m42s ago)   20h
kube-system   pod/calico-kube-controllers-6966456d6b-mml9p     1/1     Running   1 (5m42s ago)   20h
kube-system   pod/kubernetes-dashboard-585bdb5648-bqn98        1/1     Running   0               41s
kube-system   pod/metrics-server-679c5f986d-r49ql              0/1     Running   0               41s
kube-system   pod/dashboard-metrics-scraper-69d9497b54-nzj5r   1/1     Running   0               41s

NAMESPACE     NAME                                TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                  AGE
default       service/kubernetes                  ClusterIP   10.152.183.1     <none>        443/TCP                  20h
kube-system   service/kube-dns                    ClusterIP   10.152.183.10    <none>        53/UDP,53/TCP,9153/TCP   2m30s
kube-system   service/metrics-server              ClusterIP   10.152.183.106   <none>        443/TCP                  118s
kube-system   service/kubernetes-dashboard        ClusterIP   10.152.183.128   <none>        443/TCP                  96s
kube-system   service/dashboard-metrics-scraper   ClusterIP   10.152.183.64    <none>        8000/TCP                 96s

NAMESPACE     NAME                         DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
kube-system   daemonset.apps/calico-node   1         1         1       1            1           kubernetes.io/os=linux   20h

NAMESPACE     NAME                                        READY   UP-TO-DATE   AVAILABLE   AGE
kube-system   deployment.apps/calico-kube-controllers     1/1     1            1           20h
kube-system   deployment.apps/coredns                     1/1     1            1           2m30s
kube-system   deployment.apps/metrics-server              0/1     1            0           118s
kube-system   deployment.apps/kubernetes-dashboard        1/1     1            1           96s
kube-system   deployment.apps/dashboard-metrics-scraper   1/1     1            1           96s

NAMESPACE     NAME                                                   DESIRED   CURRENT   READY   AGE
kube-system   replicaset.apps/calico-kube-controllers-6966456d6b     1         1         1       20h
kube-system   replicaset.apps/coredns-64c6478b6c                     1         1         1       2m30s
kube-system   replicaset.apps/metrics-server-679c5f986d              1         1         0       41s
kube-system   replicaset.apps/kubernetes-dashboard-585bdb5648        1         1         1       41s
kube-system   replicaset.apps/dashboard-metrics-scraper-69d9497b54   1         1         1       41s
```

In the example above, the dashboard is hosted at `10.152.183.128:443`. Do not forget to prefix the URL with `https://`. 

![alt text](pictures/token.jpg)


![alt text](pictures/dashboard.png)

You can now create and deploy a configuration from this web interface.
Click on the blue plus sign in the top right corner of the page and copy the following YAML section:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app.kubernetes.io/name: load-balancer-example
  name: hello-world
spec:
  replicas: 5
  selector:
    matchLabels:
      app.kubernetes.io/name: load-balancer-example
  template:
    metadata:
      labels:
        app.kubernetes.io/name: load-balancer-example
    spec:
      containers:
      - image: gcr.io/google-samples/node-hello:1.0
        name: hello-world
        ports:
        - containerPort: 8080
```

![alt text](pictures/config_pods_hello_world.png)


Hitting the upload button above will load the given configation into the Kubernetes cluster, and is equivalent to the following command from this repository root:

```
microk8s kubectl apply -f ./hellow_world.yaml
deployment.apps/hello-world created
```

You can now look into the pods section of the web interface and see the 5 ordered replicas of the hello-world image come to life!

![alt text](pictures/hello_world_pods.png)

Clicking on one of these pods will give you all the available information on it:

* Name and UID
* CPU and memory usage
* Creation date and age
* Attached labels and annotations
* Hosting node, status and IP
* Controller name and type it belongs to
* Details on the containers inside

![alt text](pictures/pod_details.png)

You can get roughly the same information in CLI with **kubectl get** and **describe** commands:

```
microk8s kubectl get deployments hello-world
NAME          READY   UP-TO-DATE   AVAILABLE   AGE
hello-world   5/5     5            5           125m
```

```
microk8s kubectl describe deployments hello-world
Name:                   hello-world
Namespace:              default
CreationTimestamp:      Thu, 17 Mar 2022 19:50:27 +0100
Labels:                 app.kubernetes.io/name=load-balancer-example
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               app.kubernetes.io/name=load-balancer-example
Replicas:               5 desired | 5 updated | 5 total | 5 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app.kubernetes.io/name=load-balancer-example
  Containers:
   hello-world:
    Image:        gcr.io/google-samples/node-hello:1.0
    Port:         8080/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   hello-world-6755976cfc (5/5 replicas created)
Events:          <none>
```

If you click on the **Workloads** section on the side bar, you can get an overview of your cluster health:

![alt text](pictures/workload.png)

You can see this change in real-time. To check it, load another yaml configuration like the one below:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2 # tells deployment to run 2 pods matching the template
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
```

Remember: in CLI, the equivalent is:

```
microk8s kubectl apply -f ./nginx.yaml
```


![alt text](pictures/config_pods.png)

As the 2 extra pods are being launched, the workload indicators are changing: 

![alt text](pictures/pending_workload.png)

You can get an overview of all pods running with a given type of controller:

```
microk8s kubectl get replicasets
NAME                         DESIRED   CURRENT   READY   AGE
hello-world-6755976cfc       5         5         5       127m
nginx-deployment-9456bbbf9   2         2         2       11m
```

To delete a deployment, you can either use the web page:

![alt text](pictures/delete_deployment.png)

Or the CLI:

```
microk8s kubectl delete deployment nginx-deployment
deployment.apps "nginx-deployment" deleted
```

## Second example: guestbook application <a name="12"></a>

This is a simple example to deploy a PHP application using Redis and frontend instances.
Try to load the following deployments and services configurations in the guestbook folder, in this order:

1. 1_redis_db.yaml
2. 2_redis_leader_service.yaml
3. 3_redis_follower.yaml
4. 4_redis_follower_service.yaml
5. 5_frontend_deployment.yaml
6. 6_frontend_service.yaml

Remember: use the `microk8s kubectl apply -f your_cfg.yaml` to add a compoment to the Kubernetes cluster, or use the cross sign in the web dashboard to paste or give the config path.

Once the guestbooks components are up and running, you still need to expose the service on the local machine:

```
microk8s kubectl port-forward svc/frontend 8080:80
```

You can now see the guestbook at [localhost:8080](http://localhost:8080).

![alt text](pictures/guestbook.png)

**You can scale this setup up or down with just a few lines!**
To increase the number of frontend pods:

```
microk8s kubectl scale deployment frontend --replicas=5
# You can check their number increased with:
microk8s kubectl get pods
```

To bring them back to a lower level:

```
microk8s kubectl scale deployment frontend --replicas=2
```

Quite a few components were created here. To delete them all conveniently, use the label feature from Kubernetes:

```
microk8s kubectl delete deployment -l app=redis
microk8s kubectl delete service -l app=redis
microk8s kubectl delete deployment frontend
microk8s kubectl delete service frontend
```

If you removed everything properly, kubectl should now have no resource to manage:

```
microk8s kubectl get pods
No resources found in default namespace.
```

If you reached this point, **congratulations**! You are still absolutely **NOT ready** to work in the devops industry, but at least you know what infrastructure you will rely on most of the time, when you push a commit into a CI pipeline or contact a web server to get some data. 

## Author
Raphaël Bouterige
